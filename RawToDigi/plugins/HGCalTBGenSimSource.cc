#include "HGCal/RawToDigi/plugins/HGCalTBGenSimSource.h"
#include <numeric>

using namespace std;

//#define DEBUG

HGCalTBGenSimSource::HGCalTBGenSimSource(const edm::ParameterSet & pset, edm::InputSourceDescription const& desc) :  edm::ProducerSourceFromFiles(pset, desc, true),
	currentRun(-1),
	currentEvent(-1),
	rootFile(NULL)
{
	eventCounter = 0;

	//find and fill the configured runs
	RechitOutputCollectionName = pset.getParameter<std::string>("RechitOutputCollectionName");
	RunDataOutputCollectionName = pset.getParameter<std::string>("RunDataOutputCollectionName");

	produceDATURATracksInsteadOfDWCs = pset.getUntrackedParameter<bool>("produceDATURATracksInsteadOfDWCs", false);
	DWCOutputCollectionName = pset.getParameter<std::string>("DWCOutputCollectionName");
	DATURAOutputCollectionName = pset.getParameter<std::string>("DATURAOutputCollectionName");

	m_adcCalibrationsFile = pset.getUntrackedParameter<std::string>("ADCCalibrations", "HGCal/CondObjects/data/hgcal_calibration.txt");
	m_calibrationPerChannel = pset.getUntrackedParameter<bool>("calibrationPerChannel", false);

	m_maskNoisyChannels = pset.getUntrackedParameter<bool>("MaskNoisyChannels", true);
	m_channelsToMask_filename = pset.getUntrackedParameter<std::string>("ChannelsToMaskFileName", "HGCal/CondObjects/data/noisyChannels.txt");

	m_doPassiveEnergy = pset.getUntrackedParameter<bool>("DoPassiveEnergy", true);

	m_readTimingInformation = pset.getUntrackedParameter<bool>("ReadTimingInformation", true);

	energyNoise = pset.getUntrackedParameter<double>("energyNoise", 0.);	//in MIPS
	energyNoiseResolution = pset.getUntrackedParameter<double>("energyNoiseResolution", 0.);


	std::vector<double> v1(4, 1.0);
	wc_resolutions_x = pset.getUntrackedParameter<std::vector<double> >("wc_resolutions_x", v1);
	std::vector<double> v2(4, 1.0);
	wc_resolutions_y = pset.getUntrackedParameter<std::vector<double> >("wc_resolutions_y", v2);

	std::vector<double> v3(6, 1.0);
	datura_resolutions = pset.getUntrackedParameter<std::vector<double> >("datura_resolutions", v3);
	m_layerPositionFile = pset.getParameter<std::string>("layerPositionFile");

	particleGunPos_Z = pset.getUntrackedParameter<double> ("particleGunPos_Z", 0);
	beamAngleX = pset.getUntrackedParameter<double> ("beamAngleX", 0);
	beamAngleY = pset.getUntrackedParameter<double> ("beamAngleY", 0);

	beamEnergy = pset.getUntrackedParameter<double> ("beamEnergy", 250);
	beamParticlePDGID = pset.getUntrackedParameter<int> ("beamParticlePDGID", 211);
	setupConfiguration = pset.getUntrackedParameter<unsigned int> ("setupConfiguration", 1);

	switch (setupConfiguration) {
	case 1:	//July 2017
		N_layers_EE = 2;		//must shift remove layer 0 artificially
		N_layers_FH = 4;
		firstNLayersFH_asDaisies = 0;
		N_layers_BH = 12;
		break;
	case 2: //September 2017
		N_layers_EE = 7;
		N_layers_FH = 10;
		firstNLayersFH_asDaisies = 0;
		N_layers_BH = 12;
		break;
	case 3:	//October 2017 (29th May 2018: no simulated samples exist yet)
		N_layers_EE = 4;
		N_layers_FH = 6;
		firstNLayersFH_asDaisies = 6;
		N_layers_BH = 12;
		break;
	case 6:	//DESY March 2018, "CaloRuns, i.e. config #4"
	case 7:
	case 8:
	case 9:
	case 10:
		N_layers_EE = 3;
		N_layers_FH = 0;
		firstNLayersFH_asDaisies = 0;
		N_layers_BH = 0;
		break;
	case 11:	//DESY March 2018, configs #5.1/5.2
	case 12:
		N_layers_EE = 2;
		N_layers_FH = 0;
		firstNLayersFH_asDaisies = 0;
		N_layers_BH = 0;
		break;
	case 13:
	default:
		N_layers_EE = 28;
		N_layers_FH = 0;
		firstNLayersFH_asDaisies = 0;
		N_layers_BH = 0;
		break;
	case 22:					//October 2018 - setup 1, H2
		N_layers_EE = 28;
		N_layers_FH = 12;
		firstNLayersFH_asDaisies = 9;
		N_layers_BH = 12;
		maskedModules.push_back(42);		//module 42 was inactive in October 2018 configuration 1
		break;
	case 23:					//October 2018 - setup 2, H2
		N_layers_EE = 28;
		N_layers_FH = 11;
		firstNLayersFH_asDaisies = 9;
		N_layers_BH = 12;
		break;
	case 24:					//October 2018 - setup 3, H2
		N_layers_EE = 7;
		N_layers_FH = 12;
		firstNLayersFH_asDaisies = 12;
		N_layers_BH = 12;
		break;
	}

	GeVToMip300 = pset.getUntrackedParameter<double>("GeVToMip300", 1. / (85.5e-6));			
	GeVToMip200 = pset.getUntrackedParameter<double>("GeVToMip200", 1. / (57e-6));			

	areaSpecification = pset.getUntrackedParameter<std::string>("areaSpecification", "H2");
	//given in cm
	if (areaSpecification == "H6A_October2017") {
		referenceTracking_zPositions.push_back(-500.);
	} else if (areaSpecification == "H2_Summer2017") {
		referenceTracking_zPositions.push_back(-109.);
		referenceTracking_zPositions.push_back(-235.);
		referenceTracking_zPositions.push_back(-1509.);
		referenceTracking_zPositions.push_back(-1769.);
	} else if (areaSpecification == "H2") {
		referenceTracking_zPositions.push_back(-120.);
		referenceTracking_zPositions.push_back(-246.);
		referenceTracking_zPositions.push_back(-1520.);
		referenceTracking_zPositions.push_back(-1780.);
	} else if (areaSpecification == "H2_October2018") {
		referenceTracking_zPositions.push_back(-160.);
		referenceTracking_zPositions.push_back(-880.);
		referenceTracking_zPositions.push_back(-2700.);
		referenceTracking_zPositions.push_back(-3200.);
	} else if (areaSpecification == "DESY_T21_March2018") {
		referenceTracking_zPositions.push_back(0.);
		referenceTracking_zPositions.push_back(15.3);
		referenceTracking_zPositions.push_back(30.5);
		referenceTracking_zPositions.push_back(64.8);
		referenceTracking_zPositions.push_back(80.0);
		referenceTracking_zPositions.push_back(95.3);
	}


	//reads the layer positions
	std::fstream file;
	char fragment[100];
	int readCounter = -1;

	file.open(m_layerPositionFile.c_str(), std::fstream::in);
	std::cout << "Reading file " << m_layerPositionFile << " -open: " << file.is_open() << std::endl;
	int layer = 0;
	while (file.is_open() && !file.eof()) {
		readCounter++;
		file >> fragment;
		if (readCounter == 0) layer = atoi(fragment);
		if (readCounter == 1) {
			layerPositions[layer] = atof(fragment) / 10.;   //values are given in mm and should be converted into cm
			readCounter = -1;
		}
	}
	file.close();

	//indicate the physics list
	physicsListUsed = pset.getUntrackedParameter<std::string>("physicsListUsed", "");

	if (physicsListUsed == "FTF_BIC")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTF_BIC;
	else if (physicsListUsed == "FTFP_BERT")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTFP_BERT ;
	else if (physicsListUsed == "FTFP_BERT_EML")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTFP_BERT_EML ;
	else if (physicsListUsed == "FTFP_BERT_EMM")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTFP_BERT_EMM;
	else if (physicsListUsed == "FTFP_BERT_EMZ")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTFP_BERT_EMZ;
	else if (physicsListUsed == "FTFP_BERT_HP_EML")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTFP_BERT_HP_EML;
	else if (physicsListUsed == "FTFP_BERT_EMY")
		_enumPhysicsListUsed = HGCAL_TB_SIM_FTFP_BERT_EMY;
	else if (physicsListUsed == "QGSP_BERT")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_BERT;
	else if (physicsListUsed == "QGSP_BERT_EML")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_BERT_EML;
	else if (physicsListUsed == "QGSP_BERT_HP_EML")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_BERT_HP_EML;
	else if (physicsListUsed == "QGSP_FTFP_BERT")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_FTFP_BERT;
	else if (physicsListUsed == "QGSP_FTFP_BERT_EML")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_FTFP_BERT_EML;
	else if (physicsListUsed == "QGSP_FTFP_BERT_EML_New")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_FTFP_BERT_EML_New;
	else if (physicsListUsed == "QGSP_FTFP_BERT_EMM")
		_enumPhysicsListUsed = HGCAL_TB_SIM_QGSP_FTFP_BERT_EMM;
	else
		_enumPhysicsListUsed = HGCAL_TB_SIM;

	produces <HGCalTBRecHitCollection>(RechitOutputCollectionName);
	produces<RunData>(RunDataOutputCollectionName);

	if (!produceDATURATracksInsteadOfDWCs) produces<std::map<int, WireChamberData> >(DWCOutputCollectionName);
	else produces<std::vector<HGCalTBDATURATelescopeData> >(DATURAOutputCollectionName);

	if (fileNames()[0] != "file:DUMMY") {
		for (int i = 0; i < (int)(fileNames().size()); i++) {
			FileInfo fInfo;
			fInfo.index = i;
			fInfo.energy = beamEnergy;
			fInfo.pdgID = beamParticlePDGID;
			fInfo.config = setupConfiguration;
			fInfo.name = fileNames()[i];
			_fileNames.push_back(fInfo);
		}
		fileIterator = _fileNames.begin();
	}

	_e_mapFile = pset.getUntrackedParameter<std::string>("e_mapFile_CERN");
	HGCalCondObjectTextIO io(0);
	edm::FileInPath fip(_e_mapFile);

	if (!io.load(fip.fullPath(), essource_.emap_)) {
		throw cms::Exception("Unable to load electronics map");
	};

	//load the calibration files
	if (m_calibrationPerChannel) {
		fip = edm::FileInPath(m_adcCalibrationsFile);
		if (!io.load(fip.fullPath(), essource_.adccalibmap_perchannel_)) {
			throw cms::Exception("Unable to load ADC conversions map");
		};
	}


	m_detectorLayoutFile = pset.getUntrackedParameter<std::string>("DetectorLayout");
	fip = edm::FileInPath(m_detectorLayoutFile);
	if (!io.load(fip.fullPath(), essource_.layout_)) {
		throw cms::Exception("Unable to load detector layout file");
	};

	geomc = new HexGeometry(false);

	tree = 0;
	simHitCellIdEE = 0;
	simHitCellEnEE = 0;
	simHitCellIdFH = 0;
	simHitCellEnFH = 0;
	simHitCellIdBH = 0;
	simHitCellEnBH = 0;
	beamX	 = 0;
	beamY 	 = 0;
	beamP 	 = 0;
	hgcPassiveEEEnergy_ = 0;
	hgcPassiveFHEnergy_ = 0;
	hgcPassiveBHEnergy_ = 0;
	hgcPassiveBeamEnergy_ = 0;
	hgcPassiveCMSEEnergy_ = 0;

	//Variables for the timing information. 
	simHitCellTimeFirstHitEE = 0;
	simHitCellTimeFirstHitFH = 0;
	// simHitCellTimeFirstHitBH = 0;
	simHitCellTimeLastHitEE = 0;
	simHitCellTimeLastHitFH = 0;
	// simHitCellTimeLastHitBH = 0;
	simHitCellTime15MipEE = 0;
	simHitCellTime15MipFH = 0;
	// simHitCellTime15MipBH = 0;

	randgen = new TRandom();


	FILE* noisy_channelfile;
	char buffer[300];
	//edm::FileInPath fip();
	noisy_channelfile = fopen (m_channelsToMask_filename.c_str() , "r");
	if (noisy_channelfile == NULL) {
		perror ("Error opening noisy channels file"); exit(1);
	} else {

		while ( ! feof (noisy_channelfile) ) {
			if ( fgets (buffer , 300 , noisy_channelfile) == NULL ) break;
			const char* index = buffer;
			int layer, skiroc, channel, ptr, nval;
			nval = sscanf( index, "%d %d %d %n", &layer, &skiroc, &channel, &ptr );
			int skiId = HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA * layer + (HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA - skiroc) % HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA + 1;
			if ( nval == 3 ) {
				HGCalTBElectronicsId eid(skiId, channel);
				if (essource_.emap_.existsEId(eid.rawId()))
					m_noisyChannels.push_back(eid.rawId());
			} else continue;
		}
	}
	fclose (noisy_channelfile);


}


bool HGCalTBGenSimSource::setRunAndEventInfo(edm::EventID& id, edm::TimeValue_t& time, edm::EventAuxiliary::ExperimentType& evType)
{
	if (fileIterator == _fileNames.end()) {
		std::cout << "End of the files in the setRundAndEvent is reached..." << std::endl;
		delete randgen;
		return false; 		//end of files is reached
	}

	try {

		if (currentRun == -1) {		//initial loading of a file

			currentRun = (*fileIterator).index;
			currentEvent = 0;

			std::string fileName = (*fileIterator).name;
			std::string filePrefix = "file:";
			fileName.replace(fileName.find(filePrefix), filePrefix.size(), "");
			if (access( (fileName).c_str(), F_OK ) == -1) {
				std::cout << fileName << " does not exist and is skipped" << std::endl;
				fileIterator++;
				currentRun = -1;
				if (! setRunAndEventInfo(id, time, evType)) return false;
			}
			/*
			if (rootFile != NULL)
				delete rootFile;
			*/

			rootFile = new TFile(((*fileIterator).name).c_str());
			dir  = (TDirectory*)rootFile->FindObjectAny("HGCalTBAnalyzer");
			if (dir != NULL) {
				tree = (TTree*)dir->Get("HGCTB");
			} else {
				tree = (TTree*)rootFile->Get("HGCTB");
			}

			tree->SetBranchAddress("simHitCellIdEE", &simHitCellIdEE, &b_simHitCellIdEE);
			tree->SetBranchAddress("simHitCellEnEE", &simHitCellEnEE, &b_simHitCellEnEE);
			tree->SetBranchAddress("simHitCellIdFH", &simHitCellIdFH, &b_simHitCellIdFH);
			tree->SetBranchAddress("simHitCellEnFH", &simHitCellEnFH, &b_simHitCellEnFH);
			tree->SetBranchAddress("simHitCellIdBH", &simHitCellIdBH, &b_simHitCellIdBH);
			tree->SetBranchAddress("simHitCellEnBH", &simHitCellEnBH, &b_simHitCellEnBH);
			tree->SetBranchAddress("xBeam", &beamX, &b_beamX);
			tree->SetBranchAddress("yBeam", &beamY, &b_beamY);
			tree->SetBranchAddress("pBeam", &beamP, &b_beamP);

			// Add timing branches from simulation .root files.
			if (m_readTimingInformation) {
				tree->SetBranchAddress("simHitCellTimeFirstHitEE", &simHitCellTimeFirstHitEE, &b_simHitCellTimeFirstHitEE);
				tree->SetBranchAddress("simHitCellTimeFirstHitFH", &simHitCellTimeFirstHitFH, &b_simHitCellTimeFirstHitFH);
				// tree->SetBranchAddress("simHitCellTimeFirstHitBH", &simHitCellTimeFirstHitBH, &b_simHitCellTimeFirstHitBH);
				tree->SetBranchAddress("simHitCellTimeLastHitEE", &simHitCellTimeLastHitEE, &b_simHitCellTimeLastHitEE);
				tree->SetBranchAddress("simHitCellTimeLastHitFH", &simHitCellTimeLastHitFH, &b_simHitCellTimeLastHitFH);
				// tree->SetBranchAddress("simHitCellTimeLastHitBH", &simHitCellTimeLastHitBH, &b_simHitCellTimeLastHitBH);
				tree->SetBranchAddress("simHitCellTime15MipEE", &simHitCellTime15MipEE, &b_simHitCellTime15MipEE);
				tree->SetBranchAddress("simHitCellTime15MipFH", &simHitCellTime15MipFH, &b_simHitCellTime15MipFH);
				// tree->SetBranchAddress("simHitCellTime15MipBH", &simHitCellTime15MipBH, &b_simHitCellTime15MipBH);
			}

			if (m_doPassiveEnergy) {
				if (N_layers_EE > 0) tree->SetBranchAddress("hgcPassiveEEEnergy", &hgcPassiveEEEnergy_, &b_hgcPassiveEEEnergy);
				if (N_layers_FH > 0) tree->SetBranchAddress("hgcPassiveFHEnergy", &hgcPassiveFHEnergy_, &b_hgcPassiveFHEnergy);
				// if (N_layers_BH > 0) tree->SetBranchAddress("hgcPassiveBHEnergy", &hgcPassiveBHEnergy_, &b_hgcPassiveBHEnergy);
				tree->SetBranchAddress("hgcPassiveBeamEnergy", &hgcPassiveBeamEnergy_, &b_hgcPassiveBeamEnergy);
				tree->SetBranchAddress("hgcPassiveCMSEEnergy", &hgcPassiveCMSEEnergy_, &b_hgcPassiveCMSEEnergy);
			}
		}

		if (currentEvent == tree->GetEntries()) {
			fileIterator++;
			currentRun = -1;
			if (! setRunAndEventInfo(id, time, evType)) return false;
		}

		tree->GetEntry(currentEvent);

		currentEvent++;
	} catch (const std::exception& e) {
		std::cout << "Stopping readout of file: " << (*fileIterator).name << " at event " << currentEvent << std::endl;
		fileIterator++;
		currentRun = -1;
		if (! setRunAndEventInfo(id, time, evType)) return false;
	}

	return true;

}



void HGCalTBGenSimSource::produce(edm::Event & event)
{
	if (fileIterator == _fileNames.end()) {
		std::cout << "End of the files in the producer is reached..." << std::endl;
		return;
	}

	eventCounter++;	//indexes each event chronologically passing this plugin

	//first: fill the rechits
	std::unique_ptr<HGCalTBRecHitCollection> rechits(new HGCalTBRecHitCollection);

	//EE part
#ifdef DEBUG
	std::cout << "Hits in EE: " << simHitCellIdEE->size() << std::endl;
#endif
	for (unsigned int icell = 0; icell < simHitCellIdEE->size(); icell++) {
		uint32_t id = simHitCellIdEE->at(icell);
		int subdet, z, layer, wafer, celltype, cellno;
		HGCalTestNumbering::unpackHexagonIndex(id, subdet, z, layer, wafer, celltype, cellno);
		bool onDaisyStructure = false;

		if (setupConfiguration == 1)
			layer -= 1;		//remove the first layer as it is not present in the data
		if (layer == 0) continue;		//no layers with index 0 allowed
		double energy = simHitCellEnEE->at(icell);

		//Add timing reading cell by cell from TTree
		// timing time_MC;
		if(m_readTimingInformation) {
			time_MC.timeFirstHit = simHitCellTimeFirstHitEE->at(icell);
			time_MC.timeLastHit = simHitCellTimeLastHitEE->at(icell);
			time_MC.time15Mip = simHitCellTime15MipEE->at(icell);
		}

#ifdef DEBUG
		std::cout << icell << "  layer: " << layer << "   wafer:  " << wafer << "   cellno:  " << cellno << "  energy: " << energy << std::endl;
#endif

		// makeRecHit(layer, wafer, onDaisyStructure, cellno, energy, rechits);
		makeRecHit(layer, wafer, onDaisyStructure, cellno, energy, rechits, time_MC);

	}

	//FH part
#ifdef DEBUG
	std::cout << "Hits in FH: " << simHitCellIdFH->size() << std::endl;
#endif
	for (unsigned int icell = 0; icell < simHitCellIdFH->size(); icell++) {

		uint32_t id = simHitCellIdFH->at(icell);
		int subdet, z, layer, wafer, celltype, cellno;
		HGCalTestNumbering::unpackHexagonIndex(id, subdet, z, layer, wafer, celltype, cellno);
		bool onDaisyStructure = (layer <= firstNLayersFH_asDaisies);

		layer = layer + N_layers_EE;
		double energy = simHitCellEnFH->at(icell);

		//Add timing reading cell by cell from TTree
		// timing time_MC;
		if (m_readTimingInformation) {
			time_MC.timeFirstHit = simHitCellTimeFirstHitFH->at(icell);
			time_MC.timeLastHit = simHitCellTimeLastHitFH->at(icell);
			time_MC.time15Mip = simHitCellTime15MipFH->at(icell);
		}

#ifdef DEBUG
		std::cout << icell << "  layer: " << layer << "   wafer:  " << wafer << "   cellno:  " << cellno << "  energy: " << energy << std::endl;
#endif

		// makeRecHit(layer, wafer, onDaisyStructure, cellno, energy, rechits);
		makeRecHit(layer, wafer, onDaisyStructure, cellno, energy, rechits, time_MC);

	}

	event.put(std::move(rechits), RechitOutputCollectionName);


	//second: fill the run data
	std::unique_ptr<RunData> rd(new RunData);
	rd->energy = (*fileIterator).energy;		//mean energy of the beam configuration
	rd->configuration = (*fileIterator).config;
	rd->pdgID = (*fileIterator).pdgID;
	rd->runType = _enumPhysicsListUsed;
	rd->run = (*fileIterator).index;
	rd->event = eventCounter;
	rd->booleanUserRecords.add("hasDanger", false);
	rd->doubleUserRecords.add("trueEnergy", beamP);

	if (m_doPassiveEnergy) {

		double energyLostEE_ = 0.;
		double energyLostFH_ = 0.;
		double energyLostBH_ = 0.;
		double energyLostBeam_ = 0.;
		double energyLostOutside_ = 0.;

		if (N_layers_EE > 0) energyLostEE_ = std::accumulate(hgcPassiveEEEnergy_->begin(), hgcPassiveEEEnergy_->end(), 0.);
		if (N_layers_FH > 0) energyLostFH_ = std::accumulate(hgcPassiveFHEnergy_->begin(), hgcPassiveFHEnergy_->end(), 0.);
		// if (N_layers_BH > 0) energyLostBH_ = std::accumulate(hgcPassiveBHEnergy_->begin(), hgcPassiveBHEnergy_->end(), 0.);
		energyLostBeam_ = std::accumulate(hgcPassiveBeamEnergy_->begin(), hgcPassiveBeamEnergy_->end(), 0.);
		energyLostOutside_ = std::accumulate(hgcPassiveCMSEEnergy_->begin(), hgcPassiveCMSEEnergy_->end(), 0.);

		rd->doubleUserRecords.add("energyLostEE", energyLostEE_);
		rd->doubleUserRecords.add("energyLostFH", energyLostFH_);
		rd->doubleUserRecords.add("energyLostBH", energyLostBH_);
		rd->doubleUserRecords.add("energyLostBeam", energyLostBeam_);
		rd->doubleUserRecords.add("energyLostOutside", energyLostOutside_);
	}



	if (!produceDATURATracksInsteadOfDWCs) {
		//third option A: add fake dwcs from xBeam, yBeam
		std::unique_ptr<std::map<int, WireChamberData> > dwcs(new std::map<int, WireChamberData>);
		rd->booleanUserRecords.add("hasValidDWCMeasurement", true);
		for (size_t d = 0; d < referenceTracking_zPositions.size(); d++) {
			double dwc_x = beamX + tan(beamAngleX)*(referenceTracking_zPositions[d]-particleGunPos_Z);
			double dwc_y = beamY + tan(beamAngleY)*(referenceTracking_zPositions[d]-particleGunPos_Z);

			dwc_x = 10 * dwc_x + randgen->Gaus(0, wc_resolutions_x[d]);		//conversion to cm performed at a different the track computation stage
			dwc_y = 10 * dwc_y + randgen->Gaus(0, wc_resolutions_y[d]);		//conversion to cm performed at a different the track computation stage

			WireChamberData* dwc = new WireChamberData(d + 1, dwc_x , dwc_y, referenceTracking_zPositions[d]);
			dwc->goodMeasurement_X = dwc->goodMeasurement_Y = dwc->goodMeasurement = true;
			dwc->res_x = wc_resolutions_x[d];
			dwc->res_y = wc_resolutions_y[d];
			dwc->averageHitMultiplicty = 1.;

			(*dwcs)[d] = *dwc;
		}
		event.put(std::move(dwcs), DWCOutputCollectionName);
	} else {
		std::unique_ptr<std::vector<HGCalTBDATURATelescopeData> > DATURATracks(new std::vector<HGCalTBDATURATelescopeData>);
		rd->booleanUserRecords.add("hasValidDATURAMeasurement", true);
		//in this block: logical copy from HGCalTBDATURATelescopeProducer.cc
		HGCalTBDATURATelescopeData DATURATelescopeTrack(1);		//just one track by default
		LineFitter TripletTrack1X;
		LineFitter TripletTrack1Y;
		LineFitter TripletTrack2X;
		LineFitter TripletTrack2Y;
		for (int MIMOSA_index = 1; MIMOSA_index <= 6; MIMOSA_index++) {
			double x_measured = beamX + randgen->Gaus(0, datura_resolutions[MIMOSA_index - 1] / 10);		//value in cm
			double y_measured = beamY + randgen->Gaus(0, datura_resolutions[MIMOSA_index - 1] / 10);		//value in cm
			double z_measured = referenceTracking_zPositions[MIMOSA_index - 1];

			DATURATelescopeTrack.addPointForTracking(x_measured, y_measured, z_measured, datura_resolutions[MIMOSA_index - 1], datura_resolutions[MIMOSA_index - 1]);
			if (MIMOSA_index <= 3) {
				TripletTrack1X.addPoint(z_measured, x_measured, datura_resolutions[MIMOSA_index - 1]);
				TripletTrack1Y.addPoint(z_measured, y_measured, datura_resolutions[MIMOSA_index - 1]);
			} else {
				TripletTrack2X.addPoint(z_measured, x_measured, datura_resolutions[MIMOSA_index - 1]);
				TripletTrack2Y.addPoint(z_measured, y_measured, datura_resolutions[MIMOSA_index - 1]);
			}
		}

		TripletTrack1X.fit();       TripletTrack1Y.fit();
		TripletTrack2X.fit();       TripletTrack2Y.fit();

		for (std::map<int, double>::iterator layerIt = layerPositions.begin(); layerIt != layerPositions.end(); layerIt++) {
			double layer_ref_x = TripletTrack2X.eval(layerIt->second);
			double layer_ref_y = TripletTrack2Y.eval(layerIt->second);
			double layer_ref_x_chi2 = TripletTrack2X.GetChisquare();
			double layer_ref_y_chi2 = TripletTrack2Y.GetChisquare();

			//mean between two triplets for the intermediate layer
			if (layerIt->first <= 1 ) {
				layer_ref_x += TripletTrack1X.eval(layerIt->second);
				layer_ref_y += TripletTrack1Y.eval(layerIt->second);
				layer_ref_x_chi2 += TripletTrack1X.GetChisquare();
				layer_ref_y_chi2 += TripletTrack1Y.GetChisquare();

				layer_ref_x /= 2.;
				layer_ref_y /= 2.;
				layer_ref_x_chi2 /= 2.;
				layer_ref_y_chi2 /= 2.;
			}
			DATURATelescopeTrack.addLayerReference(layerIt->first, layer_ref_x, layer_ref_y, layer_ref_x_chi2, layer_ref_y_chi2);
		}

		float kinkAngleX_DUT1 = atan(TripletTrack1X.getM()) - atan(TripletTrack2X.getM());
		float kinkAngleY_DUT1 = atan(TripletTrack1Y.getM()) - atan(TripletTrack2Y.getM());
		DATURATelescopeTrack.floatUserRecords.add("kinkAngleX_DUT1", kinkAngleX_DUT1);
		DATURATelescopeTrack.floatUserRecords.add("kinkAngleY_DUT1", kinkAngleY_DUT1);
		DATURATracks->push_back(DATURATelescopeTrack);

		event.put(std::move(DATURATracks), DATURAOutputCollectionName);
	}


	event.put(std::move(rd), RunDataOutputCollectionName);
}

void HGCalTBGenSimSource::makeRecHit(int layer, int wafer, bool onDaisyStructure, int cellno, double energy, std::unique_ptr<HGCalTBRecHitCollection> &rechits, timing time_MC) {
	int cellType = geomc->cellType(cellno);

	std::pair<double, double> xy = geomc->position_cell(cellno);
	double x = xy.first;
	double y =  xy.second;

	double X = 0, Y = 0;
	if (onDaisyStructure) {
		std::pair<double, double> XY = geomc->position_wafer(wafer);
		X = XY.first;
		Y =  XY.second;
	}

	//get hexagonal coordinates
	std::pair<int, int> iuiv = TheCell.GetCellIUIVCoordinates(x, y);
	std::pair<int, int> iUiV = TheCell.GetSensorIUIVCoordinates(X, Y);
#ifdef DEBUG
	std::cout << "X: " << X << "  Y: " << Y << "    x: " << x << " y: " << y << std::endl;
	std::cout << "u: " << iuiv.first << "  v: " << iuiv.second << std::endl;
	std::cout << "U: " << iUiV.first << "  V: " << iUiV.second << std::endl;
#endif
	HGCalTBRecHit recHit(HGCalTBDetId(layer, iUiV.first, iUiV.second, iuiv.first, iuiv.second, cellType), 0., 0., 0., 0., 0);
	//attention! the electric ID to skiroc mapping takes the cellType information as well which is only 0, 2 in the simulation!
	//any analysis on the simulated data must respect that by computing MIP-ADC factors in the same way and by converting energies into MIP units
	uint32_t EID = essource_.emap_.detId2eid(recHit.id());
	HGCalTBElectronicsId eid(EID);

	HGCalTBLayer layer_ = essource_.layout_.at(recHit.id().layer() - 1);
	int moduleId = layer_.at( recHit.id().sensorIU(), recHit.id().sensorIV() ).moduleID();
	if (std::find(maskedModules.begin(), maskedModules.end(), moduleId) != maskedModules.end()) return;

	bool isNoisy = false;
	if (m_maskNoisyChannels) {
		if ( !essource_.emap_.existsEId(eid.rawId()) || std::find(m_noisyChannels.begin(), m_noisyChannels.end(), eid.rawId()) != m_noisyChannels.end() ) {
#ifdef DEBUG
			std::cout << "Masked as a noisy hit" << std::endl;
#endif
			isNoisy = true;
		}
	}
	recHit.setNoiseFlag(isNoisy);

	int skiRocIndex = eid.iskiroc_rawhit();
	skiRocIndex = skiRocIndex % HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;

	recHit.setCellCenterCoordinate(x + X, y + Y);

	if (moduleId == 144 || moduleId == 145 || moduleId == 146 || moduleId == 147) GeVToMip = GeVToMip200; //200 micron sensor for 2018 beam test
	else GeVToMip = GeVToMip300;

	energy *= GeVToMip;
	//additional noise to the energy in MIPs
	energy += randgen->Gaus(energyNoise, energyNoiseResolution);


	if (m_calibrationPerChannel) {
		int ichannel = eid.ichan();
		ASIC_ADC_Conversions_perChannel adcConv = essource_.adccalibmap_perchannel_.getASICConversions(moduleId, skiRocIndex, ichannel);
		recHit._energyHigh = energy / adcConv.adc_to_MIP();
		recHit._energyLow = recHit._energyHigh / adcConv.lowGain_to_highGain();
		recHit._energyTot = recHit._energyLow / adcConv.TOT_to_lowGain() + adcConv.TOT_offset();
		if (adcConv.fully_calibrated() == 1) recHit.setFlag(HGCalTBRecHit::kFullyCalibrated);
	} else {
		//gain assumptions from average estimates, 28th June 2018
		//1 MIP - 40 HG ADC
		//1 LG ADC - 9 HG ADC
		//1 TOT ADC - 1./0.194 LG ADC + 192 offset
		recHit._energyHigh = energy * 40;
		recHit._energyLow = recHit._energyHigh / 9.;
		recHit._energyTot = recHit._energyLow * 0.194 + 192;
	}

	recHit.setEnergy(energy);
	recHit.setFlag(HGCalTBRecHit::kGood);

	//Set time information from the simulation into recHit
	if (m_readTimingInformation) {
		recHit.setTimeFirst(time_MC.timeFirstHit);
		recHit.setTimeLast(time_MC.timeLastHit);
		recHit.setTime15Mip(time_MC.time15Mip);
	}


	rechits->push_back(recHit);
}

#include "FWCore/Framework/interface/InputSourceMacros.h"

DEFINE_FWK_INPUT_SOURCE(HGCalTBGenSimSource);
