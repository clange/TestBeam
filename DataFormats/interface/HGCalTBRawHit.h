#ifndef HGCALTBRAWHIT_H_INCLUDED
#define HGCALTBRAWHIT_H_INCLUDED 1

#include <vector>
#include "HGCal/DataFormats/interface/HGCalTBDetId.h"
#include "FWCore/Utilities/interface/TypeWithDict.h"

const static int NUMBER_OF_TIME_SAMPLES = 11;

class HGCalTBRawHit
{
 public:

  HGCalTBRawHit( ){;}
 HGCalTBRawHit( uint32_t rawid, uint16_t skiroc, uint16_t channel, std::vector<Float16_t> &adcHigh, std::vector<Float16_t> &adcLow) :
  m_rawid(rawid),
    m_skiroc(skiroc),
    m_channel(channel),
    m_adcHigh(adcHigh),
    m_adcLow(adcLow),
    m_underSaturationHG(false),
    m_underSaturationLG(false),
    m_noiseFlag(false)
    {;}
 HGCalTBRawHit( uint32_t rawid, uint16_t skiroc, uint16_t channel, std::vector<Float16_t> &adcHigh, std::vector<Float16_t> &adcLow, uint16_t toaRise, uint16_t toaFall, uint16_t totSlow, uint16_t totFast ) :
  m_rawid(rawid),
    m_skiroc(skiroc),
    m_channel(channel),
    m_adcHigh(adcHigh),
    m_adcLow(adcLow),
    m_toaRise(toaRise),
    m_toaFall(toaFall),
    m_totSlow(totSlow),
    m_totFast(totFast),
    m_underSaturationHG(false),
    m_underSaturationLG(false),
    m_noiseFlag(false)
    {;}

  HGCalTBDetId detid() const {return HGCalTBDetId(m_rawid);}
  void setRawId(uint32_t rawid){m_rawid=rawid;}
  
  uint16_t skiroc() const{return m_skiroc;}
  uint16_t channel() const{return m_channel;}
  
  void setHighGainADCs(std::vector<Float16_t> vec){m_adcHigh=vec;}
  void setLowGainADCs(std::vector<Float16_t> vec){m_adcLow=vec;}
  Float16_t highGainADC(uint16_t timeSample){return m_adcHigh.at(timeSample);}
  Float16_t lowGainADC(uint16_t timeSample){return m_adcLow.at(timeSample);}

  void setToaRise(uint16_t val){m_toaRise=val;}
  void setToaFall(uint16_t val){m_toaFall=val;}
  void setTotFast(uint16_t val){m_totFast=val;}
  void setTotSlow(uint16_t val){m_totSlow=val;}
  uint16_t toaRise(){return m_toaRise;}
  uint16_t toaFall(){return m_toaFall;}
  uint16_t totFast(){return m_totFast;}
  uint16_t totSlow(){return m_totSlow;}

  void setUnderSaturationForHighGain(){ m_underSaturationHG=true; }
  void setUnderSaturationForLowGain(){ m_underSaturationLG=true; }
  bool isUnderSaturationForHighGain(){ return m_underSaturationHG; }
  bool isUnderSaturationForLowGain(){ return m_underSaturationLG; }
  
  void setNoiseFlag(bool value){m_noiseFlag=value;}
  bool getNoiseFlag(){return m_noiseFlag;}

 private:
  uint32_t m_rawid; //for some reasons (I don't know) root does not allow saving HGCalTBDetId because of some dictionary issue. 
  // because of non-connected channels for which we assign dummy det id, we must keep skiroc and channel here
  uint16_t m_skiroc;
  uint16_t m_channel;
  std::vector<Float16_t> m_adcHigh;
  std::vector<Float16_t> m_adcLow;
  uint16_t m_toaRise;
  uint16_t m_toaFall;
  uint16_t m_totSlow;
  uint16_t m_totFast;
  bool m_underSaturationHG;
  bool m_underSaturationLG;
  bool m_noiseFlag;
};

std::ostream& operator<<(std::ostream&, HGCalTBRawHit&);

#endif
