#include <iostream>
#include "TTree.h"
#include <fstream>
#include <sstream>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "HGCal/DataFormats/interface/HGCalTBRecHitCollections.h"
#include "HGCal/DataFormats/interface/HGCalTBDetId.h"
#include "HGCal/CondObjects/interface/HGCalElectronicsMap.h"
#include "HGCal/CondObjects/interface/HGCalCondObjectTextIO.h"
#include "HGCal/CondObjects/interface/HGCalTBDetectorLayout.h"
#include "HGCal/DataFormats/interface/HGCalTBElectronicsId.h"
#include "HGCal/Geometry/interface/HGCalTBCellVertices.h"
#include "HGCal/Geometry/interface/HGCalTBTopology.h"
#include "HGCal/Geometry/interface/HGCalTBGeometryParameters.h"
#include "HGCal/DataFormats/interface/HGCalTBRunData.h" //for the runData type definition

#include <iomanip>
#include <set>

class RecHitNtupler : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
    explicit RecHitNtupler(const edm::ParameterSet&);
    ~RecHitNtupler();
    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
    virtual void beginJob() override;
    virtual void endJob() override;
    void analyze(const edm::Event& , const edm::EventSetup&) override;

    // conditions
    std::string m_electronicMap;
    std::string m_detectorLayoutFile;

    struct {
	HGCalElectronicsMap emap_;
	HGCalTBDetectorLayout layout_;
    } essource_;

    std::string m_layerPositionFile;
    std::map<int, double> layerPositions;

    // parameters
    int m_sensorsize;
    bool m_eventPlotter;
    int m_evtID;
    double m_mipThreshold;  //ineffective, 20 June 2018
    double m_noiseThreshold;
    double m_trueBeamEnergy;
    
    // ---------- member data ---------------------------
    edm::EDGetTokenT<RunData> RunDataToken;
    edm::EDGetTokenT<HGCalTBRecHitCollection> m_HGCalTBRecHitCollection;

    HGCalTBTopology IsCellValid;

    // Output tree
    TTree* tree_;

    void clearVariables(); // function to clear tree variables/vectors



    // Variables for branches

    // event info
    unsigned int ev_run_;
    unsigned int ev_event_;
    ULong64_t trigger_ts;

    int pdgID;
    float beamEnergy;
    float trueBeamEnergy;
    float energyLostEE_;
    float energyLostFH_;
    float energyLostBH_;
    float energyLostBeam_;
    float energyLostOutside_;

    unsigned int NRechits_;
    // rechits
    std::vector<unsigned int> rechit_detid_;
    std::vector<unsigned int> rechit_module_;
    std::vector<unsigned int> rechit_layer_;
    std::vector<unsigned int> rechit_chip_;
    std::vector<unsigned int> rechit_channel_;
    std::vector<unsigned int> rechit_type_;
    std::vector<Float16_t> rechit_x_;
    std::vector<Float16_t> rechit_y_;
    std::vector<Float16_t> rechit_z_;
    std::vector<short> rechit_iu_;
    std::vector<short> rechit_iv_;
    std::vector<short> rechit_iU_;
    std::vector<short> rechit_iV_;
    std::vector<Float16_t> rechit_energy_;
    std::vector<Float16_t> rechit_energy_noHG;
    std::vector<Float16_t> rechit_amplitudeHigh_;
    std::vector<Float16_t> rechit_amplitudeLow_;
    std::vector<bool> rechit_hg_goodFit;
    std::vector<bool> rechit_lg_goodFit;
    std::vector<bool> rechit_hg_saturated;
    std::vector<bool> rechit_lg_saturated;
    std::vector<bool> rechit_fully_calibrated;
    std::vector<bool> rechit_noise_flag;
    std::vector<Float16_t> rechit_TS2High_;
    std::vector<Float16_t> rechit_TS2Low_;
    std::vector<Float16_t> rechit_TS3High_;
    std::vector<Float16_t> rechit_TS3Low_;
    std::vector<unsigned int short> rechit_Tot_;
    std::vector<short> rechit_toa_calib_flag_;
    std::vector<short> rechit_toafall_flag_;
    std::vector<short> rechit_toarise_flag_;
    std::vector<Float16_t> rechit_toafall_norm_;
    std::vector<Float16_t> rechit_toarise_norm_;
    std::vector<Float16_t> rechit_toafall_time_;
    std::vector<Float16_t> rechit_toarise_time_;
    std::vector<Float16_t> rechit_toafall_corr_time_;
    std::vector<Float16_t> rechit_toarise_corr_time_;
    std::vector<Float16_t> rechit_calib_time_toafall_;
    std::vector<Float16_t> rechit_calib_time_toarise_;
    std::vector<Float16_t> rechit_timeMaxHG_;
    std::vector<Float16_t> rechit_timeMaxLG_;
    std::vector<unsigned int short> rechit_toaRise_;
    std::vector<unsigned int short> rechit_toaFall_;

    //Add variables for timing in simulation.
    std::vector<Float16_t> rechit_time_mc_firstHit_;
    std::vector<Float16_t> rechit_time_mc_lastHit_;
    std::vector<Float16_t> rechit_time_mc_;

};

void RecHitNtupler::clearVariables(){
    // event info
    ev_run_ = 0;
    ev_event_ = 0;
    energyLostEE_ = -1;
    energyLostFH_ = -1;
    energyLostBH_ = -1;
    energyLostBeam_ = -1;
    energyLostOutside_ = -1;

    NRechits_=0;
    // rechits
    rechit_detid_.clear();
    rechit_module_.clear();
    rechit_layer_.clear();
    rechit_chip_.clear();
    rechit_channel_.clear();
    rechit_type_.clear();
    rechit_x_.clear();
    rechit_y_.clear();
    rechit_z_.clear();
    rechit_iu_.clear();
    rechit_iv_.clear();
    rechit_iU_.clear();
    rechit_iV_.clear();
    rechit_energy_.clear();
    rechit_energy_noHG.clear();
    rechit_amplitudeHigh_.clear();
    rechit_amplitudeLow_.clear();
    rechit_hg_goodFit.clear();
    rechit_lg_goodFit.clear();
    rechit_hg_saturated.clear();
    rechit_lg_saturated.clear();
    rechit_fully_calibrated.clear();
    rechit_noise_flag.clear();
    rechit_TS2High_.clear();
    rechit_TS2Low_.clear();
    rechit_TS3High_.clear();
    rechit_TS3Low_.clear();
    rechit_Tot_.clear();
    rechit_toa_calib_flag_.clear();
    rechit_toafall_flag_.clear();
    rechit_toarise_flag_.clear();
    rechit_toafall_norm_.clear();
    rechit_toarise_norm_.clear();
    rechit_toafall_time_.clear();
    rechit_toarise_time_.clear();
    rechit_toafall_corr_time_.clear();
    rechit_toarise_corr_time_.clear();
    rechit_calib_time_toafall_.clear();
    rechit_calib_time_toarise_.clear();
    rechit_timeMaxHG_.clear();
    rechit_timeMaxLG_.clear();
    rechit_toaRise_.clear();
    rechit_toaFall_.clear();

    //Clear MC variables 
    rechit_time_mc_firstHit_.clear();
    rechit_time_mc_lastHit_.clear();
    rechit_time_mc_.clear();

};

RecHitNtupler::RecHitNtupler(const edm::ParameterSet& iConfig) :
    m_electronicMap(iConfig.getUntrackedParameter<std::string>("ElectronicMap","HGCal/CondObjects/data/map_CERN_Hexaboard_28Layers_AllFlipped.txt")),
    m_detectorLayoutFile(iConfig.getUntrackedParameter<std::string>("DetectorLayout","HGCal/CondObjects/data/layerGeom_oct2017_h2_17layers.txt")),
    m_layerPositionFile(iConfig.getUntrackedParameter<std::string>("layerPositionFile","")),
    m_sensorsize(iConfig.getUntrackedParameter<int>("SensorSize",128)),
    m_eventPlotter(iConfig.getUntrackedParameter<bool>("EventPlotter",false)),
    m_mipThreshold(iConfig.getUntrackedParameter<double>("MipThreshold",5.0)),
    m_noiseThreshold(iConfig.getUntrackedParameter<double>("NoiseThreshold",0.5)),
    m_trueBeamEnergy(iConfig.getUntrackedParameter<double>("trueBeamEnergy", -1))
{
    m_HGCalTBRecHitCollection = consumes<HGCalTBRecHitCollection>(iConfig.getParameter<edm::InputTag>("InputCollection"));
    RunDataToken= consumes<RunData>(iConfig.getParameter<edm::InputTag>("RUNDATA"));
    m_evtID=0;

    std::cout << iConfig.dump() << std::endl;

    HGCalCondObjectTextIO io(0);
    edm::FileInPath fip(m_electronicMap);
    if (!io.load(fip.fullPath(), essource_.emap_)) {
	throw cms::Exception("Unable to load electronics map");
    };
    fip=edm::FileInPath(m_detectorLayoutFile);
    if (!io.load(fip.fullPath(), essource_.layout_)) {
	throw cms::Exception("Unable to load detector layout file");
    };

    //read the layer positions for z-position of rechits
    std::fstream file;
    char fragment[100];
    int readCounter = -1;

    file.open(m_layerPositionFile.c_str(), std::fstream::in);

    std::cout<<"Reading file "<<m_layerPositionFile<<" -open: "<<file.is_open()<<std::endl;
    int layer=0;
    while (file.is_open() && !file.eof()) {
        readCounter++;
        file >> fragment;
        if (readCounter==0) layer=atoi(fragment);
        if (readCounter==1) {
            layerPositions[layer]=atof(fragment)/10;    //conversion to cm
            readCounter=-1;
        }
    }

    usesResource("TFileService");
    edm::Service<TFileService> fs;

    // Define tree and branches
    tree_ = fs->make<TTree>("hits", "HGC rechits");

    // event info
    tree_->Branch("event", &ev_event_);
    tree_->Branch("trigger_timestamp", &trigger_ts);
    tree_->Branch("run", &ev_run_);

    tree_->Branch("pdgID", &pdgID);
    tree_->Branch("beamEnergy", &beamEnergy);
    tree_->Branch("trueBeamEnergy", &trueBeamEnergy);
    tree_->Branch("energyLostEE", &energyLostEE_);
    tree_->Branch("energyLostFH", &energyLostFH_);
    tree_->Branch("energyLostBH", &energyLostBH_);
    tree_->Branch("energyLostBeam", &energyLostBeam_);
    tree_->Branch("energyLostOutside", &energyLostOutside_);


    tree_->Branch("NRechits", &NRechits_);
    // rechit
    tree_->Branch("rechit_detid",&rechit_detid_);
    tree_->Branch("rechit_module",&rechit_module_);
    tree_->Branch("rechit_layer",&rechit_layer_);
    tree_->Branch("rechit_chip",&rechit_chip_);
    tree_->Branch("rechit_channel",&rechit_channel_);
    tree_->Branch("rechit_type",&rechit_type_);
    tree_->Branch("rechit_x",&rechit_x_);
    tree_->Branch("rechit_y",&rechit_y_);
    tree_->Branch("rechit_z",&rechit_z_);
    tree_->Branch("rechit_iu",&rechit_iu_);
    tree_->Branch("rechit_iv",&rechit_iv_);
    tree_->Branch("rechit_iU",&rechit_iU_);
    tree_->Branch("rechit_iV",&rechit_iV_);
    tree_->Branch("rechit_energy",&rechit_energy_);
    tree_->Branch("rechit_energy_noHG",&rechit_energy_noHG);
    tree_->Branch("rechit_amplitudeHigh",&rechit_amplitudeHigh_);
    tree_->Branch("rechit_amplitudeLow",&rechit_amplitudeLow_);
    tree_->Branch("rechit_hg_goodFit", &rechit_hg_goodFit);
    tree_->Branch("rechit_lg_goodFit", &rechit_lg_goodFit);
    tree_->Branch("rechit_hg_saturated", &rechit_hg_saturated);
    tree_->Branch("rechit_lg_saturated", &rechit_lg_saturated);
    tree_->Branch("rechit_fully_calibrated", &rechit_fully_calibrated);
    tree_->Branch("rechit_noise_flag", &rechit_noise_flag);
    tree_->Branch("rechit_TS2High",&rechit_TS2High_);
    tree_->Branch("rechit_TS2Low",&rechit_TS2Low_);
    tree_->Branch("rechit_TS3High",&rechit_TS3High_);
    tree_->Branch("rechit_TS3Low",&rechit_TS3Low_);
    tree_->Branch("rechit_Tot",&rechit_Tot_);
    tree_->Branch("rechit_toa_calib_flag",&rechit_toa_calib_flag_);
    tree_->Branch("rechit_toaFall_flag",&rechit_toafall_flag_);
    tree_->Branch("rechit_toaRise_flag",&rechit_toarise_flag_);
    tree_->Branch("rechit_toaFall_norm",&rechit_toafall_norm_);
    tree_->Branch("rechit_toaRise_norm",&rechit_toarise_norm_);
    tree_->Branch("rechit_toaFall_time",&rechit_toafall_time_);
    tree_->Branch("rechit_toaRise_time",&rechit_toarise_time_);
    tree_->Branch("rechit_toaFall_corr_time",&rechit_toafall_corr_time_);
    tree_->Branch("rechit_toaRise_corr_time",&rechit_toarise_corr_time_);
    tree_->Branch("rechit_calib_time_toaFall",&rechit_calib_time_toafall_);
    tree_->Branch("rechit_calib_time_toaRise",&rechit_calib_time_toarise_);
    tree_->Branch("rechit_toaRise",&rechit_toaRise_);
    tree_->Branch("rechit_toaFall",&rechit_toaFall_);
    tree_->Branch("rechit_timeMaxHG",&rechit_timeMaxHG_);
    tree_->Branch("rechit_timeMaxLG",&rechit_timeMaxLG_);

    //Add branches for time in simulation
    tree_->Branch("rechit_time_mc_firstHit", &rechit_time_mc_firstHit_);
    tree_->Branch("rechit_time_mc_lastHit", &rechit_time_mc_lastHit_);
    tree_->Branch("rechit_time_mc", &rechit_time_mc_);

}


RecHitNtupler::~RecHitNtupler()
{

}

void RecHitNtupler::beginJob()
{
}

void RecHitNtupler::analyze(const edm::Event& event, const edm::EventSetup& setup)
{
    clearVariables();

    edm::Handle<HGCalTBRecHitCollection> rhits;
    event.getByToken(m_HGCalTBRecHitCollection, rhits);

    try {
        edm::Handle<RunData> rd;
        event.getByToken(RunDataToken, rd);

        ev_run_ = rd->run;
        ev_event_ = rd->event;
        trigger_ts = rd->trigger_ts;
        pdgID = rd->pdgID;
        beamEnergy = rd->energy;

        if(rd->doubleUserRecords.has("trueEnergy")) trueBeamEnergy=rd->doubleUserRecords.get("trueEnergy");
        else trueBeamEnergy = m_trueBeamEnergy;
        if(rd->doubleUserRecords.has("energyLostEE")) energyLostEE_ = rd->doubleUserRecords.get("energyLostEE");
        if(rd->doubleUserRecords.has("energyLostFH")) energyLostFH_ = rd->doubleUserRecords.get("energyLostFH");
        if(rd->doubleUserRecords.has("energyLostBH")) energyLostBH_ = rd->doubleUserRecords.get("energyLostBH");
        if(rd->doubleUserRecords.has("energyLostBeam")) energyLostBeam_ = rd->doubleUserRecords.get("energyLostBeam");
        if(rd->doubleUserRecords.has("energyLostOutside")) energyLostOutside_ = rd->doubleUserRecords.get("energyLostOutside");
    } catch (const std::exception& e) {
        ev_run_ = 0;
        ev_event_ = 0;
        trigger_ts = 0;
        pdgID = 0;
        beamEnergy = 0;
        trueBeamEnergy = m_trueBeamEnergy;
    }



    NRechits_ = 0;
    for( auto hit : *rhits ){
        if (hit.energy()<m_noiseThreshold) continue;
        NRechits_++;

    	// get electronics channel
    	HGCalTBElectronicsId eid( essource_.emap_.detId2eid( hit.id().rawId() ) );
        rechit_chip_.push_back(eid.iskiroc_rawhit() % HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA);
        rechit_channel_.push_back(eid.ichan());
        rechit_type_.push_back((hit.id()).cellType());

        // get layer and module ID
    	HGCalTBLayer layer = essource_.layout_.at(hit.id().layer()-1);
    	int moduleId = layer.at( hit.id().sensorIU(),hit.id().sensorIV() ).moduleID();

    	// Fill hit info and position
    	rechit_detid_.push_back(hit.id());
    	// rechit_chip_ = eid.iskiroc();
    	// rechit_channel_ = eid.ichannel();
    	rechit_module_.push_back(moduleId);
    	rechit_layer_.push_back(hit.id().layer());

    	rechit_x_.push_back( hit.cellCenter_x );
        rechit_y_.push_back( hit.cellCenter_y );
    	rechit_z_.push_back( layerPositions[hit.id().layer()] );


    	rechit_iu_.push_back( hit.id().iu() );
    	rechit_iv_.push_back( hit.id().iv() );
        rechit_iU_.push_back( hit.id().sensorIU() );
        rechit_iV_.push_back( hit.id().sensorIV() );

    	// Hit energy and time
        rechit_energy_.push_back( hit.energy() );

        rechit_energy_noHG.push_back( hit.energy_HGExcl() );


    	rechit_amplitudeHigh_.push_back( hit.energyHigh() );
    	rechit_amplitudeLow_.push_back( hit.energyLow() );
    	rechit_Tot_.push_back( hit.energyTot() );

        rechit_hg_goodFit.push_back(!(hit.checkFlag(HGCalTBRecHit::kHGFitFailed)));
        rechit_lg_goodFit.push_back(!(hit.checkFlag(HGCalTBRecHit::kLGFitFailed)));

        rechit_hg_saturated.push_back(hit.checkFlag(HGCalTBRecHit::kHighGainSaturated));
        rechit_lg_saturated.push_back(hit.checkFlag(HGCalTBRecHit::kLowGainSaturated));
        rechit_fully_calibrated.push_back(hit.checkFlag(HGCalTBRecHit::kFullyCalibrated));
        rechit_noise_flag.push_back(hit.getNoiseFlag());

        rechit_TS2Low_.push_back(hit.energyTSLow().first);
        rechit_TS2High_.push_back(hit.energyTSHigh().first);
        rechit_TS3Low_.push_back(hit.energyTSLow().second);
        rechit_TS3High_.push_back(hit.energyTSHigh().second);

        rechit_toa_calib_flag_.push_back( hit.toa_calib_flag() );
        rechit_toafall_flag_.push_back( hit.toa_flag().first );
        rechit_toarise_flag_.push_back( hit.toa_flag().second );
        rechit_toafall_norm_.push_back( hit.toa_norm().first );
        rechit_toarise_norm_.push_back( hit.toa_norm().second );
        rechit_toafall_time_.push_back( hit.toa_time().first );
        rechit_toarise_time_.push_back( hit.toa_time().second );
        rechit_toafall_corr_time_.push_back( hit.toa_corr_time().first );
        rechit_toarise_corr_time_.push_back( hit.toa_corr_time().second );
        rechit_calib_time_toafall_.push_back( hit.toa_calib_time().first );
        rechit_calib_time_toarise_.push_back( hit.toa_calib_time().second );
        rechit_timeMaxHG_.push_back( hit.timeMaxHG() );
    	rechit_timeMaxLG_.push_back( hit.timeMaxLG() );

        rechit_toaRise_.push_back( hit.toaRise() );
        rechit_toaFall_.push_back( hit.toaFall() );

        //Add to the ntuples the information from the time in simulation. 
        //To do so we have to add the methods to get these information in the HGCalRecHit.hh class
        rechit_time_mc_firstHit_.push_back( hit.timeFirstHit() );
        rechit_time_mc_lastHit_.push_back( hit.timeLastHit() );
        rechit_time_mc_.push_back( hit.time15Mip() );

    } // end rechit loop

    tree_->Fill();
}


void RecHitNtupler::endJob()
{
}

void RecHitNtupler::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
    edm::ParameterSetDescription desc;
    desc.setUnknown();
    descriptions.addDefault(desc);
}

DEFINE_FWK_MODULE(RecHitNtupler);
