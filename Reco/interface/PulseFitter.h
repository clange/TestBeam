#ifndef HGCAL_PULSEFITTER
#define HGCAL_PULSEFITTER

#include <vector>
#include <cmath>
#include <iostream>

#include <Math/Minimizer.h>
#include <Math/Factory.h>
#include <Math/Functor.h>
#include "FWCore/Utilities/interface/TypeWithDict.h"

struct PulseFitterResult{
PulseFitterResult() : tmax(0.), amplitude(0.), chi2(1e6), 
    errortmax(0.), erroramplitude(0.), status(-1), ncalls(10000) {;}
  double tmax;
  double amplitude;
  double chi2;
  double trise;
  double errortmax;
  double erroramplitude;
  double errorchi2;
  int status;
  int ncalls;
};

struct fitterParameter{
fitterParameter():tmax0(140),
    tmaxRangeUp(200),
    tmaxRangeDown(10),
    nMaxIterations(100)
  {;}
  double tmax0;
  double tmaxRangeUp;
  double tmaxRangeDown;
  int nMaxIterations;
};

class PulseFitter{
 public:
  PulseFitter( int printLevel, int fitmodel_version=2, double maxTime=225 , double trise=35 , double ampl_norm=1.85 , double tau=20 , int n_ord=3 );
  ~PulseFitter(){;}
  void run(std::vector<Float16_t> &time, std::vector<Float16_t> &energy, PulseFitterResult &fit, Float16_t noise=-1);
  void setFitterParameter( fitterParameter params ){ m_fitterParameter=params; }
 private:
  int m_printLevel;
  fitterParameter m_fitterParameter;
  double (*fitModelPointer)(const double *x);

};

#endif
