#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/program_options.hpp>

#include <HGCal/CondObjects/interface/HGCalCondObjects.h>
#include <HGCal/DataFormats/interface/HGCalTBDetId.h>
#include <HGCal/Geometry/interface/HGCalTBGeometryParameters.h>

#include <TFile.h>
#include <TTree.h>

int main(int argc,char**argv)
{
  std::string m_outputname,m_dbname;
  try { 
    namespace po = boost::program_options; 
    po::options_description desc("Options"); 
    desc.add_options() 
      ("help,h", "Print help messages") 
      ("outputName", po::value<std::string>(&m_outputname)->default_value("db.root"), "name of output root file")
      ("DBName", po::value<std::string>(&m_dbname)->default_value("myDB.db"), "name of data base file ");
      
    po::variables_map vm; 
    try { 
      po::store(po::parse_command_line(argc, argv, desc),  vm); 
      if ( vm.count("help")  ) { 
        std::cout << desc << std::endl; 
        return 0; 
      } 
      po::notify(vm);
    }
    catch(po::error& e) { 
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      std::cerr << desc << std::endl; 
      return 1; 
    }
    if( vm.count("DBName") ) std::cout << "DBName = " << m_dbname << std::endl;
    if( vm.count("outputName") ) std::cout << "outputName = " << m_outputname << std::endl;
    
  }
  catch(std::exception& e) { 
    std::cerr << "Unhandled Exception reached the top of main: " 
              << e.what() << ", application will now exit" << std::endl; 
    return 2; 
  } 

  HGCalTBCondDB db;
  std::ifstream ifs(m_dbname);
  boost::archive::text_iarchive ia(ifs);
  ia >> db;

  TFile* file=new TFile(m_outputname.c_str(),"RECREATE");
  TTree* tree=new TTree("hgcaltb_dbtree","hgcaltb_dbtree");
  
  uint32_t detid;
  uint32_t elecid;
  uint16_t module;
  float z;
  std::vector<float> pedestalHG, pedestalLG, noiseHG, noiseLG;
  float hgMIP, hgSAT;
  float lgMIP, lgSAT;
  float totCOEF, totTHR, totPED, totNORM, totPOW;

  tree->Branch("detid",&detid);
  tree->Branch("elecid",&elecid);
  tree->Branch("module",&module);
  tree->Branch("z",&z);
  tree->Branch("pedestalHG",&pedestalHG);
  tree->Branch("pedestalLG",&pedestalLG);
  tree->Branch("noiseHG",&noiseHG);
  tree->Branch("noiseLG",&noiseLG);
  tree->Branch("hgMIP",&hgMIP);
  tree->Branch("hgSAT",&hgSAT);
  tree->Branch("lgMIP",&lgMIP);
  tree->Branch("lgSAT",&lgSAT);
  tree->Branch("totCOEF",&totCOEF);
  tree->Branch("totTHR",&totTHR);
  tree->Branch("totPED",&totPED);
  tree->Branch("totNORM",&totNORM);
  tree->Branch("totPOW",&totPOW);

  for( std::vector<HGCalTBCondChannel>::iterator it=db.get().begin(); it!=db.get().end(); ++it ){
    detid=(*it).detid();
    elecid=(*it).elecid();
    module=(*it).module();
    z=(*it).z();
    pedestalHG=(*it).pedestalHG();
    pedestalLG=(*it).pedestalLG();
    noiseHG=(*it).noiseHG();
    noiseLG=(*it).noiseLG();
    hgMIP=(*it).highGainMIP();
    hgSAT=(*it).highGainSAT();
    lgMIP=(*it).lowGainMIP();
    lgSAT=(*it).lowGainSAT();
    totCOEF=(*it).totCOEF();
    totTHR=(*it).totTHR();
    totPED=(*it).totPED();
    totNORM=(*it).totNORM();
    totPOW=(*it).totPOW();    
    tree->Fill();
  } 
  
  tree->Write();
  file->Close();

  return 0;
}
